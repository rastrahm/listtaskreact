import React from 'react';
import AppPage from './AppPage'

export default class AppLink extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            link: this.props.link
        };
    }
	
    componentDidMount() {
        this.setState({ link: this.props.link })
	}
	
    render() {
        return <AppPage link={this.state.link} />
    }
}
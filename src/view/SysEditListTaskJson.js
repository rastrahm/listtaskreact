import React from 'react';
import {
	Button, 
    ButtonGroup,
    Form, 
    FormGroup, 
    Label
} from 'reactstrap';
import {AiOutlineCheck} from '@react-icons/all-files/ai/AiOutlineCheck'
import {AiOutlineClose} from '@react-icons/all-files/ai/AiOutlineClose'
import ReactJson from 'react-json-view';

export default class SysEditListTaskJson extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            type: props.type,
            jwt: props.jwt,
            hide: props.hide,
            route: props.route,
			big_id: props.data.big_id,
            txt_json: JSON.parse(props.data.txt_group)
		}
        this.handleChangeGroup = this.handleChangeGroup.bind(this);
    }

    mySubmitHandler(values) {
        var message = {
            menu: values.route,
            big_id: values.big_id,
            txt_json: values.txt_json
        };
        let method = "PUT";
        if (values.type === 'new') {
            method = "POST";
        } 
		fetch(window.location.protocol + "//" + window.location.hostname + "/functions/" + values.route, {
			method: method,
			mode: "cors", 
			cache: "no-cache", 
			credentials: "same-origin", 
			headers: {
				"Content-Type": "application/json",
                "jwt": values.jwt
			},
			redirect: "follow", 
			referrerPolicy: "no-referrer",
			body: JSON.stringify(message)
		})
		.then(res => res.json())
		.then(
			(result) => {
                if (result.data[0].result === 't') {
                    debugger;
                    //hide showalert
                    values.hide();
                }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
			alert('Error de comunicación');
        })
    }

    handleChangeGroup(event) {
        this.setState({txt_json: event.updated_src});
    }

    render() {
        return (
            <div>
                <Form>
                    {this.state.type === 'edit' ? 
                        <FormGroup>
                            <Label for="id">ID</Label><h3>{this.state.big_id}</h3>
                        </FormGroup>
                        :
                        <FormGroup>
                            <h3>Elemento Nuevo</h3>
                        </FormGroup>
                    }
                    <FormGroup>
                        <Label for="json">ID</Label><h3>Editar Tareas</h3>
                        <ReactJson name="json" 
                                    id="json" 
                                    enableClipboard 
                                    displayObjectSize 
                                    src={this.state.txt_json} 
                                    onEdit={(edit)=>{this.handleChangeGroup(edit)}} 
                                    onAdd={(edit)=>{this.handleChangeGroup(edit)}} 
                                    theme="monokai"/>
                    </FormGroup>
                    <ButtonGroup style={{float: 'right'}}>
                        <Button color="primary" onClick={() => {this.mySubmitHandler(this.state)}}><AiOutlineCheck/></Button>
                        <Button color="danger" onClick={this.props.hide}><AiOutlineClose /></Button>
                    </ButtonGroup>
                </Form>
            </div>
        )
    }
}
